package lt.kvk.i15.krutikovas;

import java.util.ArrayList;
import java.util.Comparator;

public class Person extends BTreeElementAbstract{
	private int id;
	private String username;
	private String password;
	private String email;
	private ArrayList<Integer> ticketNo;
	
	public Person(String username, String password, String email, ArrayList<Integer> tn)
	{
		this.username = username;
		this.password = password;
		this.email = email;
		id = IdCounter.generateId();
		ticketNo = tn;
		super.setWeight(id);
	}
	public String getUsername()
	{
		return username;
	}
	
	public String getEmail()
	{
		return email;
	}
	
	public ArrayList<Integer> getTickets()
	{
		return ticketNo;
	}






}
