package lt.kvk.i15.krutikovas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Node<T extends BTreeElementAbstract> implements Comparator<Node<T>> {
	private BTreeInfo info;
	private Node<T> parent;
	private boolean isRoot;
	private boolean hasChildren;
	private List<Node<T>> children = new ArrayList<Node<T>>();
	private List<T> valueList = new ArrayList<T>();
	private int minValue;
	private int maxValue;

	// add new node/leaf
	public Node(T value, BTreeInfo info, Node<T> parent) {
		this.info = info;
		valueList.add(value);
		isRoot = false;
		this.parent = parent;
		minValue = value.getWeight();
		maxValue = value.getWeight();
	}

	// init stage of root
	public Node(BTreeInfo info) {
		this.info = info;
		isRoot = true;
		hasChildren = false;
		parent = null;
	}

	public List<T> getValues() {
		return valueList;
	}

	private void insert(T value) {
		valueList.add(value);
		Collections.sort(valueList, valueList.get(0));
		if (children.size() > 0)
			Collections.sort(children, children.get(0));
		findMinMax();
		if (valueList.size() > info.getLapElKiekisMax())
		{
			if (!isRoot)
			{
				T temp = valueList.get(info.getEile() + 1);
				valueList.remove(info.getEile() + 1);
				findMinMax();
				parent.children.add(new Node<T>(temp, info, parent));
				int n = valueList.size();
				for (int i = info.getEile() + 1; i < n; i++)
				{
					temp = valueList.get(info.getEile() + 1);
					valueList.remove(info.getEile() + 1);
					parent.children.get(parent.children.size() - 1).insert(temp);
				}
				if (children.size() > info.getAtsakosMax()) {
					for (int i = 0; i < info.getEile() + 1; i++) {
						children.get(info.getEile() + 1).setParent(parent.children.get(parent.children.size() - 1));
						parent.children.get(parent.children.size() - 1).children.add(children.get(info.getEile() + 1));
						children.remove(info.getEile() + 1);
					}
				}
				temp = valueList.get(info.getEile());
				valueList.remove(info.getEile());
				parent.insert(temp);
			} 
			else if (isRoot && children.size() >= info.getAtsakosMax())
			{
				T temp = valueList.get(0);
				valueList.remove(0);
				Node<T> child1 = new Node(temp, info, this), child2;
				children.add(child1);

				for (int i = 0; i < info.getEile() + 1; i++) {
					children.get(0).setParent(children.get(children.size() - 1));
					children.get(children.size() - 1).children.add(children.get(0));
					children.remove(0);
				}
				for (int i = 1; i < info.getEile(); i++)
				{
					temp = valueList.get(0);
					valueList.remove(0);
					child1.insert(temp);
				}
				temp = valueList.get(1);
				valueList.remove(1);
				child2 = new Node<T>(temp, info, this);
				children.add(child2);
				for (int i = 0; i < info.getEile() + 1; i++) {
					children.get(0).setParent(children.get(children.size() - 1));
					children.get(children.size() - 1).children.add(children.get(0));
					children.remove(0);
				}
				for (int i = 1; i < info.getEile(); i++) {
					temp = valueList.get(1);
					valueList.remove(1);
					child2.insert(temp);
				}

			} 
			else if (isRoot && !hasChildren)
			{
				T temp = valueList.get(0);
				valueList.remove(0);
				Node<T> n1 = new Node<T>(temp, info, this), n2;
				children.add(n1);
				for (int i = 1; i < info.getEile(); i++)
				{
					temp = valueList.get(0);
					valueList.remove(0);
					children.get(0).insert(temp);
				}

				temp = valueList.get(valueList.size() - 1);
				valueList.remove(valueList.size() - 1);
				n2 = new Node<T>(temp, info, this);
				children.add(n2);
				for (int i = 1; i < info.getEile(); i++)
				{
					temp = valueList.get(1);
					valueList.remove(1);
					children.get(1).insert(temp);
				}

			}
		}

		if (children.size() > 0)
			Collections.sort(children, children.get(0));
		Collections.sort(valueList, valueList.get(0));
		findMinMax();
		hasChildren = children.size() != 0;

	}

	public void add(T value) {
		if (isRoot && valueList.size() == 0) {
			insert(value);
			return;
		}
		hasChildren = children.size() != 0;
		int weight = value.getWeight();
		findMinMax();
		if (hasChildren) {
			if (weight <= minValue) {
				children.get(0).add(value);
			} else if (weight >= maxValue) {
				children.get(children.size() - 1).add(value);
			} else {
				boolean isAdded = false;
				for (int i = 1; i < valueList.size() - 1; i++) {
					if (valueList.get(i).getWeight() > weight) {
						children.get(i).add(value);
						isAdded = true;
					}
				}
				if (!isAdded) {
					children.get(children.size() - 2).add(value);
				}
			}

		} else {
			insert(value);
		}
	}

	public T search(int weight) {
		T obj = null;
		if ((!hasChildren && weight > maxValue) || (!hasChildren && weight < minValue)) {
			return obj;
		}
		if (weight < minValue) {
			children.get(0).search(weight);
		} else if (weight > maxValue) {
			children.get(children.size() - 1).search(weight);
		} else {

			for (T v : valueList) {
				if (weight == v.getWeight()) {
					return v;
				}
			}
			for (int i = 0; i < valueList.size() - 1; i++) {
				obj = valueList.get(i);
				if (weight == obj.getWeight()) {
					return obj;
				} else if (weight > valueList.get(i).getWeight()) {
					if (!hasChildren) {
						return null;
					} else {
						children.get(i + 1).search(weight);
					}
				}

			}
		}
		return obj;
	}

	public void remove(int weight) {
		T obj = null;
		if ((!hasChildren && weight > maxValue) || (!hasChildren && weight < minValue)) {
			return;
		}
		if (weight < minValue) {
			children.get(0).remove(weight);
		} else if (weight > maxValue) {
			children.get(children.size() - 1).remove(weight);
		} else {
			int index = 0;
			for (T v : valueList) {
				if (weight == v.getWeight()) {
					valueList.remove(index);
					if (children.size() > 0)
						Collections.sort(children, children.get(0));
					Collections.sort(valueList, valueList.get(0));
					findMinMax();
					hasChildren = children.size() != 0;
					if (valueList.size() == 0)
						parent.children.remove(this);
					return;
				}
				index++;
			}
			for (int i = 0; i < valueList.size() - 1; i++) {
				obj = valueList.get(i);
				if (weight == obj.getWeight()) {
					valueList.remove(i);
					if (children.size() > 0)
						Collections.sort(children, children.get(0));
					Collections.sort(valueList, valueList.get(0));
					findMinMax();
					hasChildren = children.size() != 0;
					if (valueList.size() == 0)
						parent.children.remove(this);
					return;
				} else if (weight > valueList.get(i).getWeight()) {
					if (!hasChildren) {
						return;
					} else {
						children.get(i + 1).remove(weight);
					}
				}

			}
		}
	}

	private void findMinMax() {
		int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
		for (int i = 0; i < valueList.size(); i++) {
			if (valueList.get(i).getWeight() > max)
				max = valueList.get(i).getWeight();
			if (valueList.get(i).getWeight() < min)
				min = valueList.get(i).getWeight();
		}
		minValue = min;
		maxValue = max;
	}

	public boolean isRoot() {
		return isRoot;
	}

	public boolean hasChildren() {
		return hasChildren;
	}

	public int getChildrenCount() {
		return children.size();
	}

	public int getMinValue() {
		return minValue;
	}

	public int getMaxValue() {
		return maxValue;
	}

	private void setParent(Node<T> n) {
		parent = n;
	}

	private boolean maxValueCount() {
		if (isRoot) {
			if (info.getSaknyjeElKiekisMax() == valueList.size())
				return true;
			else
				return false;
		} else if (info.getLapElKiekisMax() == valueList.size()) {
			return true;
		}
		return false;
	}

	@Override
	public int compare(Node<T> o1, Node<T> o2) {
		if (o1.minValue > o2.minValue) {
			return 1;
		} else if (o1.minValue < o2.minValue) {
			return -1;
		}
		return 0;
	}

}
