package lt.kvk.i15.krutikovas;

public class BTree<T extends BTreeElementAbstract>{
	private Node<T> root = null;
	private BTreeInfo info;
	
	public BTree(int eile)
	{
		info = new BTreeInfo(eile);
		root = new Node<T>(info);
	}
	
	public void add(T value)
	{
		root.add(value);
	}
	
	public T searchInTree(int weight)
	{
		T obj = root.search(weight);
		return obj;
	}
	
	public void remove(int weight)
	{
		root.remove(weight);
	}
	
	public void remove(T obj)
	{
		root.remove(obj.getWeight());
	}
}
