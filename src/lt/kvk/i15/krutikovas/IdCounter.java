package lt.kvk.i15.krutikovas;

public class IdCounter {
	private static int newestId = -1;
	
	public static int generateId()
	{
		newestId++;
		return newestId;
	}
}
