package lt.kvk.i15.krutikovas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import lt.kvk.i15.krutikovas.files.FileReader;
import lt.kvk.i15.krutikovas.files.FileWriterSimple;
import lt.kvk.i15.krutikovas.simpleUI.SimpleUI;

public class BTreeMain {
	public static BTree<Person> m1;
	public static BTree<Person> m2;

	public static void main(String[] args) {
		List<BTreeElementAbstract> people = new ArrayList<BTreeElementAbstract>();
		
		List<Person> ppl = FileReader.readPersonFile();
		for(Person p: ppl)
			people.add(p);
		Collections.shuffle(people);
		m1 = new BTree<Person>(1);
		m2  = new BTree<Person>(2);
		for(BTreeElementAbstract p: people)
		{
			m1.add((Person)p);
		}
		for(BTreeElementAbstract p: people)
		{
			m2.add((Person)p);	
		}
		SimpleUI.startUI(people);
			
			System.out.println("program ended successfully!");

	}

}
