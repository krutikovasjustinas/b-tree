package lt.kvk.i15.krutikovas.files;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import lt.kvk.i15.krutikovas.Person;

public class FileReader {
	public static ArrayList<Person> readPersonFile()
	{
		ArrayList<Person> p = new ArrayList<Person>();
		Scanner s;
		try {
			s = new Scanner(new File("src/lt/kvk/i15/krutikovas/files/duom.txt"));
			while(s.hasNext())
			{
				String username = s.next(), password = s.next(), email = s.next();
				ArrayList<Integer> tickets = new ArrayList<Integer>();
				int n = s.nextInt();
				for(int i = 0; i < n; i++)
				{
					tickets.add(s.nextInt());
				}
				p.add(new Person(username, password, email, tickets));
			}
			s.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return p;
	}
}
