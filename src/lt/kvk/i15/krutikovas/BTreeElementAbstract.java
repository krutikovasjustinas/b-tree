package lt.kvk.i15.krutikovas;

import java.util.Comparator;

public abstract class BTreeElementAbstract implements Comparator<BTreeElementAbstract> {
	private int weight;

	public int compare(BTreeElementAbstract a, BTreeElementAbstract b) {
		if (a.weight > b.weight) {
			return 1;
		} else if (a.weight < b.weight) {
			return -1;
		}
		return 0;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int w) {
		weight = w;
	}
}
