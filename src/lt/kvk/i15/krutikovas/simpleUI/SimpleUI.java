package lt.kvk.i15.krutikovas.simpleUI;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import lt.kvk.i15.krutikovas.BTreeElementAbstract;
import lt.kvk.i15.krutikovas.BTreeMain;
import lt.kvk.i15.krutikovas.Person;
import lt.kvk.i15.krutikovas.files.FileWriterSimple;

public class SimpleUI {
	public static void startUI(List<BTreeElementAbstract> people)
	{
		Scanner sc;
		try {
			boolean auto = false;
			sc = new Scanner(System.in);
			while(true)
			{
				System.out.println("ready for your command!");
				String command = sc.next();
				if(command.substring(0, 3).equalsIgnoreCase("add"))
				{
					String username, password, email;
					ArrayList<Integer> tickets = new ArrayList<Integer>();
					System.out.println("Adding a person:\n Tell us his username");
					username = sc.next();
					System.out.println("Password?:");
					password = sc.next();
					System.out.println("Email?:");
					email = sc.next();
					if(auto)
					{
						Random r = new Random();
						int rn = r.nextInt(9)+1;
						for(int i = 0; i < rn; i++)
							tickets.add(r.nextInt(900000)+100000);
					}
					else {
						System.out.println("enter number of tickets");
						int rn = sc.nextInt();
						for(int i = 0; i < rn; i++)
							tickets.add(sc.nextInt());
					}
					Person pers = new Person(username, password, email, tickets);
					String wf = "\n"+username+ " " + password + " " + email + " " + tickets.size();
					for(Integer i: tickets)
						wf += " " + i;
					FileWriterSimple.write(wf);
					people.add(pers);
					BTreeMain.m1.add(pers);
					BTreeMain.m2.add(pers);
					System.out.println("added a new person!");
				}
				else {
					if(command.substring(0, 4).equalsIgnoreCase("auto"))
					{
						auto = !auto;
						if(auto)
							System.out.println("auto mode on");
						else 
							System.out.println("auto mode off");
					}
					if(command.substring(0, 4).equalsIgnoreCase("exit"))
						break;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
