package lt.kvk.i15.krutikovas;

public class BTreeInfo {
	private int eile;
	private int saknyjeElKiekisMin;
	private int saknyjeElKiekisMax;
	private int atsakosMin;
	private int atsakosMax;
	private int lapElKiekisMin;
	private int lapElKiekisMax;
	
	
	public BTreeInfo(int eile) {
		this.eile = eile;
		saknyjeElKiekisMin = 1;
		saknyjeElKiekisMax = eile*2;
		atsakosMin = 2;
		atsakosMax = (eile * 2) + 1;
		lapElKiekisMin = eile;
		lapElKiekisMax = eile*2;
	}
	
	public int getEile() {
		return eile;
	}
	public int getSaknyjeElKiekisMin() {
		return saknyjeElKiekisMin;
	}
	public int getSaknyjeElKiekisMax() {
		return saknyjeElKiekisMax;
	}
	public int getAtsakosMin() {
		return atsakosMin;
	}
	public int getAtsakosMax() {
		return atsakosMax;
	}
	public int getLapElKiekisMin() {
		return lapElKiekisMin;
	}
	public int getLapElKiekisMax() {
		return lapElKiekisMax;
	}
	
	
}
